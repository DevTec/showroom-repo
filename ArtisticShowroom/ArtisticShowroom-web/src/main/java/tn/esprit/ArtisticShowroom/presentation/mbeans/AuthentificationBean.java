package esprit.ArtisticShowroom.presentation.mbeans;

import java.util.ArrayList;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import esprit.ArtisticShowroom.persistence.Artist;
import esprit.ArtisticShowroom.persistence.Client;
import esprit.ArtisticShowroom.persistence.User;
import esprit.ArtisticShowroom.services.IUserservices;

@ManagedBean
@SessionScoped
public class AuthentificationBean {
	private String login;
	private String password;
	private User user;
	
	@EJB
	 private IUserservices userB;
	

	public String login() {
	
		user= userB.authentificate(login, password);
		if (user != null) {
			if(user instanceof Artist)
			{
				return"";
			}
			
			else if(user instanceof Client)
				return"";
			
		}
		
		
		return "login";
	}

}
