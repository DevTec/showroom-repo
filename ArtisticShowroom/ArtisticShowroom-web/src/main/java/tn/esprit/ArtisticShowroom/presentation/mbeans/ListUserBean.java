package esprit.ArtisticShowroom.presentation.mbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import esprit.ArtisticShowroom.persistence.Artist;
import esprit.ArtisticShowroom.persistence.User;
import esprit.ArtisticShowroom.services.IUserservices;
@ManagedBean (name="userbean")
@ViewScoped
public class ListUserBean implements Serializable {
	
	
	@EJB
	IUserservices userB;
	private List<User> artists;
	private Artist artiste;
	private User user;
	private String text1;  
	 
	@PostConstruct
	 public void init(){
		artiste = new Artist();
		user =new User();
		artists= new ArrayList<>();
		artists=userB.recupereUser();
		 
	 }


	public User getUser() {
		return user;
	}



	public void setUser(User user) {
		this.user = user;
	}



	public String suppUser()
	{  
		userB.suppUser(user);;
		return"";
	}

	 public String modifUser()
	 {
		 userB.uppArtist(artiste);
		 return "";
	 }
	 
	 
	public String getText1() {
		return text1;
	}


	public void setText1(String text1) {
		this.text1 = text1;
	}


	public List<User> getArtists() {
		return artists;
	}

	public void setArtists(List<User> artists) {
		this.artists = artists;
	}


	public Artist getArtiste() {
		return artiste;
	}


	public void setArtiste(Artist artiste) {
		this.artiste = artiste;
	}

 	
	
	
	
	
	
	

}
