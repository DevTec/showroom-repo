package tn.esprit.ArtisticShowroom.presentation.mbeans;


import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import interfaces.SpecialtyInterfaceLocal;
import persistence.Specialty;

@ManagedBean
@RequestScoped
public class ManageSpecialtyBean {
	private String description;

	@EJB
	SpecialtyInterfaceLocal specialtylocal;

	public String addSpecialty() {
		Specialty specialty = new Specialty();
		specialty.setDescription(description);
		specialtylocal.AddSpecialty(specialty);
		return "/manager/AffectSpecialty";

	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
