package tn.esprit.ArtisticShowroom.presentation.mbeans;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import interfaces.CategoryInterfaceLocal;
import persistence.Category;

@ManagedBean
@ViewScoped
public class ManageCategoryBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name; 
	private int id;
	private Boolean renderUpdateForm = false;
	private Category category;
	private List<Category> categories;
	private List<Category> filteredCat;
	 
	@EJB
    CategoryInterfaceLocal categoryLocal;
	
	
	public String addCategory() {
		Category category = new Category();
		category.setName(name);
		categoryLocal.AddCategory(category);
		return "/manager/ListCategory";

	}
	
	@PostConstruct
	public void init() {
		category = new Category();
		categories = new ArrayList<>();
		categories = categoryLocal.recupererListCategories();
		System.out.println(categories.size());
	}
	public String UpdateCategory() {
		categoryLocal.UpdateCategory(category);    
		return "";
	}
	
	public String supprimerCategory() {
		categoryLocal.deleteCategory(category);
	    category = new Category();
		init();
		return null;
	}
    
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Boolean getRenderUpdateForm() {
		return renderUpdateForm;
	}

	public void setRenderUpdateForm(Boolean renderUpdateForm) {
		this.renderUpdateForm = renderUpdateForm;
	}

	public List<Category> getFilteredCat() {
		return filteredCat;
	}

	public void setFilteredCat(List<Category> filteredCat) {
		this.filteredCat = filteredCat;
	}




	
}
