package esprit.ArtisticShowroom.presentation.mbeans;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FlowEvent;

import esprit.ArtisticShowroom.persistence.Artist;
import esprit.ArtisticShowroom.persistence.Client;
import esprit.ArtisticShowroom.persistence.User;
import esprit.ArtisticShowroom.persistence.Owner;
import esprit.ArtisticShowroom.services.IUserservices;



@ManagedBean(name="inscrib")
@ViewScoped
public class UserBean implements Serializable {
    @EJB
	IUserservices userB;
    private String nom;
    private String b;
    private String c;
    private String e;
    private String k;
    private int experience ;
	
	 private boolean skip;
	 private User user = new User();
	 private Artist artist = new Artist();
	 private Client client = new Client();
	 private Owner Owner = new Owner();
	 
 public String adduser(){
		    Artist a = new Artist();
			a.setFirstName(nom);
			a.setPassword(c);
			a.setLogin(b);
			a.setDescription(e);
			a.setLastName(k);
			a.setExperience(experience);
			userB.addArtist(a);
			
			return "/Detailusers";
		}
	 
	 
 
	 
	public String getC() {
	return c;
}




public void setC(String c) {
	this.c = c;
}




public String getE() {
	return e;
}




public void setE(String e) {
	this.e = e;
}




public String getK() {
	return k;
}




public void setK(String k) {
	this.k = k;
}




public int getExperience() {
	return experience;
}




public void setExperience(int experience) {
	this.experience = experience;
}




	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getB() {
		return b;
	}



	public void setB(String b) {
		this.b = b;
	}



	public Artist getArtist() {
		return artist;
	}



	public void setArtist(Artist artist) {
		this.artist = artist;
	}



	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Owner getOwner() {
		return Owner;
	}
	public void setOwner(Owner owner) {
		Owner = owner;
	}
	public boolean isSkip() {
		return skip;
	}
	public void setSkip(boolean skip) {
		this.skip = skip;
	}
	 
	 public void save() {        
	        FacesMessage msg = new FacesMessage("Successful", "Welcome :" + artist.getFirstName());
	        FacesContext.getCurrentInstance().addMessage(null, msg);
	        userB.addArtist(artist);
	    }
	
	 public String onFlowProcess(FlowEvent event) {
	        if(skip) {
	            skip = false;   //reset in case user goes back
	            return "confirm";
	        }
	        else {
	            return event.getNewStep();
	        }
	    }
	 
	 
	 
	

}
