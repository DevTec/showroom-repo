package interfaces;

import javax.ejb.Remote;

import persistence.Artist;
import persistence.Specialty;

@Remote
public interface SpecialtyInterfaceRemote {
	
	void AddSpecialty(Specialty specialty);
	
	public Artist findArtistByName(int id);

	void affecterSpecialtyToArtist(Artist artist, Specialty specialty);

}
