package interfaces;

import java.util.List;

import javax.ejb.Local;

import persistence.Category;

@Local
public interface CategoryInterfaceLocal {

	void AddCategory(Category category);

	void UpdateCategory(Category category);

	void deleteCategory(Category category);

	Category findCategory(int id);

	List<Category> getAll();

	List<Category> recupererListCategories();

}
