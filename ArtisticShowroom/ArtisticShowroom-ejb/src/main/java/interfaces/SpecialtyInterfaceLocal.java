package interfaces;

import javax.ejb.Local;

import persistence.Artist;
import persistence.Specialty;

@Local
public interface SpecialtyInterfaceLocal {

	void AddSpecialty(Specialty specialty);

	public Artist findArtistByName(int id);

	void affecterSpecialtyToArtist(Artist artist, Specialty specialty);

}
