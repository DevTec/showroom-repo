package interfaces;

import java.util.List;

import javax.ejb.Remote;

import persistence.Category;

@Remote
public interface CategoryInterfaceRemote {

	void AddCategory(Category category);

	void UpdateCategory(Category category);

	void deleteCategory(Category category);

	Category findCategory(int id);

	List<Category> getAll();

}
