package persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Survey
 *
 */
@Entity

public class Survey implements Serializable {

	
	private int id;
	private String note;
	private int count;
	private Exposure exposure ;
	private List<QuestionSurvey> Questions;
	private static final long serialVersionUID = 1L;

	public Survey() {
		super();
	}   
	
	
	
	@OneToMany
	public List<QuestionSurvey> getQuestions() {
		return Questions;
	}

	public void setQuestions(List<QuestionSurvey> questions) {
		Questions = questions;
	}




	@OneToOne(mappedBy="survey")
	public Exposure getExposure() {
		return exposure;
	}


	public void setExposure(Exposure exposure) {
		this.exposure = exposure;
	}

	@Id    
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note = note;
	}   
	public int getCount() {
		return this.count;
	}

	public void setCount(int count) {
		this.count = count;
	}
   
}
