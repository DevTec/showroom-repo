package persistence;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: QuestionSurvey
 *
 */
@Entity

public class QuestionSurvey implements Serializable {

	
	private int id;
	private String question;
	private AnswerSurvey Answer;
	private Survey survey;
	private static final long serialVersionUID = 1L;

	public QuestionSurvey() {
		super();
	} 
	
	
	
	@ManyToOne
	public Survey getSurvey() {
		return survey;
	}




	public void setSurvey(Survey survey) {
		this.survey = survey;
	}




	@Id    
	public int getId() {
		return this.id;
	}

	@OneToOne(mappedBy="questionSurvey")
	public AnswerSurvey getAnswer() {
		return Answer;
	}




	public void setAnswer(AnswerSurvey answer) {
		Answer = answer;
	}




	public void setId(int id) {
		this.id = id;
	}   
	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}
   
}
