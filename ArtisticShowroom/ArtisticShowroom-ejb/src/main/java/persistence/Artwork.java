package persistence;

import java.io.File;
import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Artwork
 *
 */
@Entity

public class Artwork implements Serializable {

	
	private int Id;
	private String description;
	private float price;
	private File photo;
	private Artist artist;
	private BookingPurchasing bookingPurchasing ;
	private Category category;
	private List<Details> details;
	private static final long serialVersionUID = 1L;

	public Artwork() {
		super();
	} 
	
	@OneToMany(mappedBy="artwork")
	public List<Details> getDetails() {
		return details;
	}


	@OneToOne
	public BookingPurchasing getBookingPurchasing() {
		return bookingPurchasing;
	}

	public void setBookingPurchasing(BookingPurchasing bookingPurchasing) {
		this.bookingPurchasing = bookingPurchasing;
	}

	public void setDetails(List<Details> details) {
		this.details = details;
	}


	@ManyToOne
	public Category getCategory() {
		return category;
	}



	public void setCategory(Category category) {
		this.category = category;
	}



	@ManyToOne
	public Artist getArtist() {
		return artist;
	}



	public void setArtist(Artist artist) {
		this.artist = artist;
	}



	public File getPhoto() {
		return photo;
	}

	public void setPhoto(File photo) {
		this.photo = photo;
	}
	
	@Id    
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public float getPrice() {
		return this.price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
   
}
