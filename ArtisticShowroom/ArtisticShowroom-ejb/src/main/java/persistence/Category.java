package persistence;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Category
 *
 */
@Entity

public class Category implements Serializable {

	
	private int Id;
	private String name;
	private List<Artwork> artworks;
	private static final long serialVersionUID = 1L;

	
	
	
	public Category() {
		super();
	}

	public Category(int id, String name, List<Artwork> artworks) {
		super();
		Id = id;
		this.name = name;
		this.artworks = artworks;
	}

	@OneToMany
	public List<Artwork> getArtworks() {
		return artworks;
	}

	public void setArtworks(List<Artwork> artworks) {
		this.artworks = artworks;
	}


	@Id    
	@GeneratedValue
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}   
	
}
