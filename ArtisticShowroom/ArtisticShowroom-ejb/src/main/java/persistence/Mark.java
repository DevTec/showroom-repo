package persistence;

import javax.persistence.Entity;

@Entity
public class Mark {
private MarkPK markPK;
private Client client;
private Artist artist;
public MarkPK getMarkPK() {
	return markPK;
}
public void setMarkPK(MarkPK markPK) {
	this.markPK = markPK;
}
public Client getClient() {
	return client;
}
public void setClient(Client client) {
	this.client = client;
}
public Artist getArtist() {
	return artist;
}
public void setArtist(Artist artist) {
	this.artist = artist;
}
}
