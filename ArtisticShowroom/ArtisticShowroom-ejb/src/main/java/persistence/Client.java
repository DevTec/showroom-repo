package persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import persistence.User;

/**
 * Entity implementation class for Entity: Client
 *
 */
@Entity

public class Client extends User implements Serializable {

	private List<BookingPurchasing> bookingpurchasing;
	private static final long serialVersionUID = 1L;

	public Client() {
		super();
	}

	@OneToMany
	public List<BookingPurchasing> getBookingpurchasing() {
		return bookingpurchasing;
	}

	public void setBookingpurchasing(List<BookingPurchasing> bookingpurchasing) {
		this.bookingpurchasing = bookingpurchasing;
	}
   
	
	
	
}
