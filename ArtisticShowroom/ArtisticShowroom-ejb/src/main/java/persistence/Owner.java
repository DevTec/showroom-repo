package persistence;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import persistence.User;

/**
 * Entity implementation class for Entity: Owner
 *
 */
@Entity

public class Owner extends User implements Serializable {

	private List<Metting> mettings;
	private  List<Space> spaces;
	private static final long serialVersionUID = 1L;

	public Owner() {
		super();
	}

	@OneToMany
	public List<Space> getSpaces() {
		return spaces;
	}

	public void setSpaces(List<Space> spaces) {
		this.spaces = spaces;
	}
	@OneToMany(mappedBy="owner")
	public List<Metting> getMettings() {
		return mettings;
	}

	public void setMettings(List<Metting> mettings) {
		this.mettings = mettings;
	}
   
	
	
	
}
