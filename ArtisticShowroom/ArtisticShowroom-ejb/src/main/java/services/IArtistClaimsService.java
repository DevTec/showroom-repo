package esprit.ArtisticShowroom.services;

import java.util.List;

import javax.ejb.Remote;

import esprit.ArtisticShowroom.persistence.Claims;
@Remote
public interface IArtistClaimsService {
	void AddClaim(Claims c);
	void DeleteClaims(Claims claim);
	void UpdateClaims(Claims claim);
	Claims FindById(int ClaimId);
	List<Claims>DisplayListClaims();
	List<Claims>FindBySubject(String S);
	
}
