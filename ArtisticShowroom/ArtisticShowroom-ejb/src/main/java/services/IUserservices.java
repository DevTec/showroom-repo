package esprit.ArtisticShowroom.services;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;

import esprit.ArtisticShowroom.persistence.Administrator;
import esprit.ArtisticShowroom.persistence.Artist;
import esprit.ArtisticShowroom.persistence.Client;
import esprit.ArtisticShowroom.persistence.Owner;
import esprit.ArtisticShowroom.persistence.User;





@Local
public interface IUserservices {
	
	
	boolean addUser(User user);
	User authentificate(String login, String password);
	void addArtist(Artist artist);
	void addOwner(Owner owner);
	void suppUser(User user);
	void addClient(Client client);
	void addAministrateur(Administrator admin);
	void suppArtist(Artist artist);
	void suppOwner(Owner owner);
	void suppClient(Client client);
	void suppAministrateur(Administrator admin);
	void uppArtist(Artist artist);
	void uppOwner(Owner owner);
	void uppClient(Client client);
	void uppAministrateur(Administrator admin);
	
	List<Artist> recupererListArtists();

	List<Artist> chercherArtistParNom(String nom);

	List<Artist> chercherArtistParNomEtExperiences(String nom, Integer e);
	List<User> recupereUser();
	
    
}
