package esprit.ArtisticShowroom.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import esprit.ArtisticShowroom.persistence.*;

@Stateless
public class ArtistClaimsService implements IArtistClaimsService{

	@PersistenceContext
	private EntityManager em;
	@Override
	public void AddClaim(Claims c) {
		em.persist(c);
		
	}
	
	@Override
	public void DeleteClaims(Claims claim) {
		claim = em.find(Claims.class, claim.getId());
		em.remove(claim);
		
	}

	@Override
	public void UpdateClaims(Claims claim) {
		em.merge(claim);
		
		
	}

	@Override
	public Claims FindById(int ClaimId) {
		return em.find(Claims.class, ClaimId);
	}

	@Override
	public List<Claims> DisplayListClaims() {
		TypedQuery<Claims> query=em.createQuery("select c from Claims c", Claims.class);
		return query.getResultList();
	}

	@Override
	public List<Claims> FindBySubject(String S) {
		TypedQuery<Claims> query=em.createQuery("select c from Claims c where c.subject=:S", Claims.class);
		query.setParameter("S", S);
		return query.getResultList();
	}
  
}
