package esprit.ArtisticShowroom.services;

import java.util.List;

import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import esprit.ArtisticShowroom.persistence.Administrator;
import esprit.ArtisticShowroom.persistence.Artist;
import esprit.ArtisticShowroom.persistence.Client;
import esprit.ArtisticShowroom.persistence.Owner;
import esprit.ArtisticShowroom.persistence.User;




@Stateless
public class Userservices implements IUserservices {
	@PersistenceContext
	EntityManager em;
	@Override
	public boolean addUser(User user) {
		try {
			em.persist(user);
			return true;
		} catch (Exception e) {
			return false;
		}}
	

	@Override
	public User authentificate(String login, String password) {
		
		User User =null;
		Query query = em.createQuery("select a from User a where a.login=:l and a.password=:p");  
		query.setParameter("l", login).setParameter("p", password);
		try {
			User =(User) query.getSingleResult();
		} catch (Exception e) {
			User = null;
		}
		return User;}


	@Override
	public void addArtist(Artist artist) {
		em.persist(artist);
		
	}


	@Override
	public void addOwner(Owner owner) {
		em.persist(owner);

		
	}


	@Override
	public void addClient(Client client) {
		em.persist(client);

	}


	@Override
	public void addAministrateur(Administrator admin) {
		em.persist(admin);

	}


	@Override
	public List<User> recupereUser() {
		TypedQuery<User> query = em.createQuery("select h from User h", User.class);
		return query.getResultList();
	}


	@Override
	public void suppArtist(Artist artist) {
		
		em.remove(artist);
		
		
	}


	@Override
	public void suppOwner(Owner owner) {
		em.remove(owner);
		
	}


	@Override
	public void suppClient(Client client) {
		em.remove(client);
		
	}


	@Override
	public void suppAministrateur(Administrator admin) {
		em.remove(admin);
		
	}


	@Override
	public void uppArtist(Artist artist) {
		em.merge(artist);
		// TODO Auto-generated method stub
		
	}


	@Override
	public void uppOwner(Owner owner) {
		em.merge(owner);
		// TODO Auto-generated method stub
		
	}


	@Override
	public void uppClient(Client client) {
		em.merge(client);
		
		
	}


	@Override
	public void uppAministrateur(Administrator admin) {
		
		em.merge(admin);
		
		
	}


	@Override
	public List<Artist> recupererListArtists() {
		String type="artist";
		TypedQuery<Artist> query = em.createQuery("select h from User h where h.type=:type", Artist.class);
		query.setParameter("type",type );
		return query.getResultList();
	}


	@Override
	public List<Artist> chercherArtistParNom(String nom) {
		String type="artist";
		TypedQuery<Artist> query = em.createQuery("select h from User h where h.type=:type and firstname=:nom", Artist.class);
		query.setParameter("nom",nom );
		query.setParameter("type",type );
		return query.getResultList();
		
	}


	@Override
	public List<Artist> chercherArtistParNomEtExperiences(String nom, Integer exp) {
		String type="artist";
		TypedQuery<Artist> query = em.createQuery("select h from User h where h.type=:type and h.firstname=:nom and h.experience=:e ", Artist.class);
		query.setParameter("type",type );
		query.setParameter("nom",nom );
		query.setParameter("e",exp );
		return query.getResultList();
		
	}


	@Override
	public void suppUser(User user) {
      User u = em.find(User.class, user.getId());
		em.remove(user);
	}


	
	
	}


