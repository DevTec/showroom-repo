package services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import interfaces.CategoryInterfaceLocal;
import interfaces.CategoryInterfaceRemote;
import persistence.Artwork;
import persistence.Category;

@Stateless
@LocalBean
public class CategoryService implements CategoryInterfaceRemote, CategoryInterfaceLocal {
	@PersistenceContext
	EntityManager em;

	/**
	 * Default constructor.
	 */
	public CategoryService() {

	}

	@Override
	public void AddCategory(Category category) {
		em.persist(category);

	}

	@Override
	public void UpdateCategory(Category category) {
		em.merge(category);
	}

	@Override
	public void deleteCategory(Category category) {
		System.out.println("categorie id : "+category.getId());
		category = em.find(Category.class, category.getId());
		//for (Artwork a : category.getArtworks()) {
			//a.setCategory(null);
			//em.merge(a);
		//}
		em.remove(category);
	}

	@Override
	public Category findCategory(int id) {
		return em.find(Category.class, id);
	}

	@Override
	public List<Category> getAll() {
		TypedQuery<Category> query = em.createQuery("select c from Category c", Category.class);
		return query.getResultList();
	}

	@Override
	public List<Category> recupererListCategories() {
		TypedQuery<Category> querry = em.createQuery("select c from Category c",Category.class);
		return querry.getResultList();
	}

}