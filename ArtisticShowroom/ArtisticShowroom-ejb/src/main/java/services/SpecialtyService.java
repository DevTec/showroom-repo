package services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import interfaces.SpecialtyInterfaceLocal;
import interfaces.SpecialtyInterfaceRemote;
import persistence.Artist;
import persistence.Specialty;

@Stateless
@LocalBean
public class SpecialtyService implements SpecialtyInterfaceRemote, SpecialtyInterfaceLocal {
	@PersistenceContext
	EntityManager em;

	/**
	 * Default constructor.
	 */

	public SpecialtyService() {

	}

	@Override
	public void AddSpecialty(Specialty specialty) {
		em.persist(specialty);

	}

	@Override
	public Artist findArtistByName(int id) {

		return em.find(Artist.class, id);
	}

	@Override
	public void affecterSpecialtyToArtist(Artist artist, Specialty specialty) {
		em.persist(specialty);
		artist = em.find(Artist.class, artist.getId());
		artist.getSpecialtys().add(specialty);

	}

}
