package presentation;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import interfaces.CategoryInterfaceRemote;
import persistence.Category;

public class ListCategory {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		CategoryInterfaceRemote Icategory = (CategoryInterfaceRemote) context.lookup(
				"ArtisticShowroom-ear/ArtisticShowroom-ejb/CategoryService!interfaces.CategoryInterfaceRemote");
		
		List<Category> c = Icategory.getAll();
		
		for (Category category : c) {
			System.out.println(category.getName());
		}
	}

}
