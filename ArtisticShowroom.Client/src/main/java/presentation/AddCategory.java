package presentation;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


import interfaces.CategoryInterfaceRemote;
import persistence.Category;

public class AddCategory {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		CategoryInterfaceRemote Icategory = (CategoryInterfaceRemote) context.lookup("ArtisticShowroom-ear/ArtisticShowroom-ejb/CategoryService!interfaces.CategoryInterfaceRemote");
		
		Category c = new Category();
		c.setName("peinture");
		Icategory.AddCategory(c);
		
		Category c2 = new Category();
		c2.setName("sculture");
		Icategory.AddCategory(c2);
		
		Category c3 = new Category();
		c3.setName("architecture");
		Icategory.AddCategory(c3);
		
		Category c4 = new Category();
		c4.setName("litterature");
		Icategory.AddCategory(c4);

	}

}
