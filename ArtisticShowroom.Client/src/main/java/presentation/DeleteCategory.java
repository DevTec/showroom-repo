package presentation;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


import interfaces.CategoryInterfaceRemote;
import persistence.Category;

public class DeleteCategory {

	public static void main(String[] args) throws NamingException {
		Context context = new InitialContext();
		CategoryInterfaceRemote Icategory = (CategoryInterfaceRemote) context.lookup(
				"ArtisticShowroom-ear/ArtisticShowroom-ejb/CategoryService!interfaces.CategoryInterfaceRemote");
		Category c = new Category();
		c.setId(1);
		Icategory.deleteCategory(c);
	}

}
